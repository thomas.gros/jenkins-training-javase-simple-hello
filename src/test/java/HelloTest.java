import org.junit.Test;

import static org.junit.Assert.*;
public class HelloTest {

    @Test
    public void helloShouldReturnHello(){
        Hello hello = new Hello();

        assertEquals("hello should return hello", "hello", hello.hello());
    }
}
